package com.zenika.academy.barbajavas.wordle.controller;

import com.zenika.academy.barbajavas.wordle.application.GameManager;
import com.zenika.academy.barbajavas.wordle.domain.model.Game;
import com.zenika.academy.barbajavas.wordle.domain.repository.GameRepository;
import com.zenika.academy.barbajavas.wordle.domain.service.BadLengthException;
import com.zenika.academy.barbajavas.wordle.domain.service.IllegalWordException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class GameController {

    private final GameManager gameManager;


    @Autowired
    public GameController(GameManager gameManager){
        this.gameManager = gameManager;

    }

    @PostMapping("/games")
    public Game game(@RequestParam(value= "wordLength", defaultValue = "5") int wordLength, @RequestParam(value = "nbAttempts", defaultValue = "6" ) int nbAttempts){
        return  gameManager.startNewGame(wordLength, nbAttempts);

    }

    @PostMapping("/games/{gameTId}")
    public Game game(@PathVariable String gameTId, @RequestBody Guess guess) throws BadLengthException, IllegalWordException {
        return gameManager.attempt(gameTId, guess.getGuess());
    }

    @GetMapping("/games/{gameTId}")
    public Game gameCheck(@PathVariable String gameTId) {
        return gameManager.findGame(gameTId);
    }

//    @GetMapping("{gameTid}")
//    ResponseEntity<Game> getGame(@PathVariable String gameTid){
//        Optional<Game> optionalGame = gameManager.getGame(gameTid);
//
//        return gameManager.getGame(gameTid)
//                .map((Game game) -> ResponseEntity.ok(game))
//                .orElseGet(ResponseEntity.notFound().build());
//    }

}
