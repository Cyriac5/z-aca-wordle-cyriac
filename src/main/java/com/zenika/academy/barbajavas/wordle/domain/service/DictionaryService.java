package com.zenika.academy.barbajavas.wordle.domain.service;

import org.springframework.stereotype.Component;

public interface DictionaryService {
    String getRandomWord(int length);
    boolean wordExists(String word);
}
