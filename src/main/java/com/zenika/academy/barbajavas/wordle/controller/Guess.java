package com.zenika.academy.barbajavas.wordle.controller;

import com.fasterxml.jackson.annotation.JsonCreator;

public class Guess {
    private final String guess;

    @JsonCreator
    public Guess(String guess){
        this.guess = guess;
    }

    public String getGuess() {
        return guess;
    }


}
