package com.zenika.academy.barbajavas.wordle.exceptions;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.context.request.WebRequest;

public class ApiError {
    String error;
    HttpStatus status;


    public ApiError(String error, HttpStatus status) {
        this.error = error;
        this.status = status;
    }

    public String getError() {
        return error;
    }

}
