package com.zenika.academy.barbajavas.wordle.exceptions;

import com.zenika.academy.barbajavas.wordle.domain.service.BadLengthException;
import com.zenika.academy.barbajavas.wordle.domain.service.IllegalWordException;
import com.zenika.academy.barbajavas.wordle.domain.service.i18n.I18n;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.util.WebUtils;

import java.util.Collections;
import java.util.List;

@ControllerAdvice
public class ControllerExceptions {

    @Autowired
    private I18n i18n;

    @ExceptionHandler({BadLengthException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected ResponseEntity<ApiError> handleBadLengthException(BadLengthException ex){
        ApiError apiError = new ApiError(this.i18n.getMessage("nb_letters_word_try"), HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);

    }

    @ExceptionHandler({IllegalArgumentException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected ResponseEntity<ApiError> handleIllegalArgumentException(IllegalArgumentException ex){

        ApiError apiError = new ApiError(this.i18n.getMessage("game_is_not_existing"), HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);

    }

    @ExceptionHandler({IllegalWordException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected ResponseEntity<ApiError> handleIllegalWordException(IllegalWordException ex){

        ApiError apiError = new ApiError(this.i18n.getMessage("word_not_in_dictionary"), HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);

    }


//    protected ResponseEntity<ApiError> handleExceptionInternal(ApiError body, HttpStatus status) {
//        return new ResponseEntity<ApiError>(body, status);
//    }
}
